<?php

namespace Personal;

require_once "classes/Database.php";
require_once "classes/User.php";
use DB\Database;

require_once "includes/helpers.php";

class UserManager
{
    /**
     * Get user with supplied params
     * @param $params
     * @return User|null
     * @throws UserException
     */
    public static function get($params)
    {
        $db = Database::getConnection();
        $filter_params = [
            'id' => 'id',
            'username' => 'username',
            'email' => 'email',
            'full_name' => 'full_name'
        ];
        $sql_params = "";
        foreach ($filter_params as $filer_param => $sql_param)
        {
            if (array_key_exists($filer_param, $params))
            {
                $sql_params.= "$filer_param=:$sql_param";
            }
        }
        if (!$sql_params)
        {
            throw new \Exception("No suitable parameters supplied");
        }

        $select_query = "SELECT * FROM `users` WHERE ".$sql_params;
        $statement = $db->prepare($select_query);
        $statement->execute($params);
        $result = $statement->fetch();
        if (!$result){
            return null;
        }

        $user = new User($result["username"], $result["email"], $result["full_name"]);
        $user->password_encrypted = $result["password_hash"];
        return $user;
    }

    /**
     * Get user and check if supplied password is correct
     * @param $username
     * @param $password
     * @return User|null
     * @throws UserException
     */
    public static function authenticate($username, $password)
    {
        $user = self::get(["username" => $username]);
        if (!$user)
        {
            throw new UserException("Incorrect username or password");
        }
        if (validatePassword($password, $user->password_encrypted))
        {
            throw new UserException("Incorrect username or password");
        }
        return $user;
    }

    /**
     * Create new user
     * @param $username
     * @param $email
     * @param $full_name
     * @param $password
     * @return User
     * @throws UserException
     */
    public static function create($username, $email, $full_name, $password)
    {
        $new_user = new User($username, $email, $full_name);
        $new_user->setPassword($password);
        $new_user->save();
        return $new_user;
    }
}