<?php
namespace Personal;
require_once "classes/Database.php";
use DB\Database;

require_once "includes/helpers.php";
/**
 * Class User
 */
class User
{
    private $_id;
    public $username;
    public $email;
    public $full_name;
    public $password_encrypted;

    const USERNAME_MAX_LENGTH = 15;
    const USERNAME_MIN_LENGTH = 3;
    const FULL_NAME_MAX_LENGTH = 15;
    const FULL_NAME_MIN_LENGTH = 3;

    /**
     * User constructor.
     * @param string $username user name
     * @param string $email email
     * @param string $full_name full name (first name, second name)
     * @throws UserException
     */
    public function __construct($username, $email, $full_name)
    {
        $errors = $this->_validate_data($username, $email, $full_name);
        if (!empty($errors)) {
            throw new UserException(join('\n', $errors));
        }
        $this->username = $username;
        $this->email = $email;
        $this->full_name = $full_name;

    }

    /**
     * Validate supplied user params
     * @param $username
     * @param $email
     * @param $full_name
     * @return array
     */
    private function _validate_data($username, $email, $full_name)
    {
        $errors = array();

        if (mb_strlen($username) > self::USERNAME_MAX_LENGTH) {
            $errors[] = "Username is longer than". self::USERNAME_MIN_LENGTH ." symbols";
        }
        if (mb_strlen($username) < self::USERNAME_MIN_LENGTH) {
            $errors[] = "Username is less than ". self::USERNAME_MIN_LENGTH ." symbols";
        }

        if (!ctype_alnum($username)) {
            $errors[] = "Username may consist only letters or digits";
        }

        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $errors[] = "Invalid email";
        }

        if (mb_strlen($full_name) > self::FULL_NAME_MAX_LENGTH) {
            $errors[] = "Full name is longer than ". self::FULL_NAME_MAX_LENGTH ." symbols";
        }
        if (mb_strlen($full_name) < self::FULL_NAME_MIN_LENGTH) {
            $errors[] = "Full name is less than ". self::FULL_NAME_MIN_LENGTH ." symbols";
        }
        return $errors;
    }

    /**
     * Hash password
     * @param string $password cleartext password
     */
    public function setPassword($password)
    {
        $this->password_encrypted = hashPassword($password);
    }

    /**
     * Create new user or update existing user params
     * @throws UserException
     */
    public function save()
    {
        $db = Database::getConnection();

        if (!$this->_id) {
            $select_query = "SELECT * FROM `users` WHERE username=:username OR email=:email";
            $statement = $db->prepare($select_query);
            $statement->execute(['username' => $this->username, 'email' => $this->email]);
            $result = $statement->fetch();
            if ($result) {
                if ($result["username"] == $this->username) {
                    $matching_param = "username";
                } else {
                    $matching_param = "email";
                }
                throw new UserException("User with this $matching_param already exists");
            }

            $data = [
                'username'  => $this->full_name,
                'email'     => $this->email,
                'full_name' => $this->full_name,
                'password_hash'  => $this->password_encrypted
            ];
            $insert_query = "INSERT INTO `users` (username, email, full_name, password_hash) 
            VALUES (:username, :email, :full_name, :password_hash)";

            $statement = $db->prepare($insert_query);
            $statement->execute($data);
            $this->_id = $db->lastInsertId();
        } else {
            $data = [
                'id'            => $this->_id,
                'full_name'     => $this->full_name,
                'password_hash' => $this->password_encrypted
            ];
            $update_query = "UPDATE `users` SET full_name=:full_name, password_hash=:password_hash 
            WHERE id=:id";

            $statement = $db->prepare($update_query);
            $statement->execute($data);
        }
    }

}