<?php

namespace DB;
use PDO;

class Database
{
    private static $_link = null;

    /**
     * Create database connection
     * @return PDO|null
     */
    public static function getConnection()
    {
        if (self::$_link != null) {
            return self::$_link;
        }

        $db_host = getenv('DB_HOST');
        $db_name = getenv('DB_NAME');
        $db_user = getenv('DB_USER');
        $db_password = getenv('DB_PASSWORD');
        $db_charset = getenv('DB_CHARSET');

        $db_dsn = "mysql:host=$db_host;dbname=$db_name;charset=$db_charset";
        $db_options = [
            PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
            PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
            PDO::ATTR_EMULATE_PREPARES   => false,
        ];

        self::$_link = new PDO($db_dsn, $db_user, $db_password, $db_options);
        return self::$_link;
    }

    public static function closeConnection()
    {
        self::$_link = null;
    }
}