<?php

require_once "classes/UserException.php";
require_once "classes/UserManager.php";
use Personal\UserException;
use Personal\UserManager;

session_start();

if ($_SERVER['REQUEST_METHOD'] === 'POST')
{
    $error_message = null;
    try
    {
        $current_user = UserManager::authenticate($_POST['username'], $_POST['password']);
        $_SESSION['user'] = $current_user->username;
        header("Location: index.php");
    } catch (UserException $e)
    {
        $error_message = $e->getMessage();
    }

}

$welcome_message = null;
if (isset($_GET['new_user']))
{
    $welcome_message = "Registration successful. You can login now.";
}

include_once "templates/login_form.html";