<?php

/**
 * Get hash of cleartext password
 * @param $password
 * @return false|string|null
 */
function hashPassword($password)
{
    return password_hash($password, PASSWORD_BCRYPT);
}

/**
 * Check if cleartext password is matching supplied hash
 * @param $clearTextPassword
 * @param $passwordHash
 * @return bool
 */
function validatePassword($clearTextPassword, $passwordHash)
{
    return hashPassword($clearTextPassword) == $passwordHash;
}