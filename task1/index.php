<?php
session_start();
require_once "classes/UserException.php";
require_once "classes/UserManager.php";
use Personal\UserException;
use Personal\UserManager;

if (isset($_SESSION['user'])) {
    try {
        $current_user = UserManager::get(["username" => $_SESSION['user']]);
    } catch (UserException $e) {
        unset($_SESSION["user"]);
        header("Location: index.php");
    }
    include_once "templates/client_area.html";
} else {
    include_once "templates/index_page.html";
}


?>