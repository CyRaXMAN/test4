<?php

require_once "classes/UserException.php";
require_once "classes/UserManager.php";
use Personal\UserException;
use Personal\UserManager;

if ($_SERVER['REQUEST_METHOD'] === 'POST')
{
    $new_user = null;
    $error_message = null;
    try
    {
        $new_user = UserManager::create(
            $_POST['username'], $_POST['email'], $_POST['full_name'], $_POST['password']
        );
    } catch (UserException $e)
    {
        $error_message = $e->getMessage();
    }

    if ($new_user)
    {
        header("Location: /login.php?new_user");
        die();
    }
}

include "templates/registration_form.html";