
create table users
(
    id    int auto_increment
        primary key,
    email varchar(55) null,
    login varchar(55) null
);

create table orders
(
    id      int auto_increment
        primary key,
    user_id int null,
    price   int null,
    constraint orders_users_id_fk
        foreign key (user_id) references users (id)
            on update cascade on delete cascade
);

