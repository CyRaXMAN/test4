#!/usr/bin/env bash

SOCKET="/var/lib/mysql/mysql.sock"

mysqld --skip-networking --socket="$SOCKET" --user=mysql &>/dev/null &
for i in {15..0}; do
  if mysqladmin --socket="$SOCKET" ping &>/dev/null; then
    break
  fi
  echo 'Waiting for server...'
  sleep 1
done

if [ "$i" = 0 ]; then
  echo >&2 'Timeout during MySQL init.'
  exit 1
fi

echo "Running tests..."
for sql_file in test-1.sql test-2.sql test-3.sql; do
  mysql -u $MYSQL_USER -p$MYSQL_PASSWORD $MYSQL_DATABASE --socket="$SOCKET" < $sql_file
  if [ $? -eq 0 ]; then
    echo "$sql_file SUCCESSFULL"
  else
    echo "$sql_file FAILED"
  fi
done
mysqladmin -p$MYSQL_ROOT_PASSWORD --socket="$SOCKET" shutdown