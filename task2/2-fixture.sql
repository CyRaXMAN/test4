INSERT INTO task2.users (id, email, login) VALUES (1, 'user1@localhost', 'user1');
INSERT INTO task2.users (id, email, login) VALUES (2, 'user2@localhost', 'user2');
INSERT INTO task2.users (id, email, login) VALUES (3, 'user1@localhost', 'user4');
INSERT INTO task2.orders (id, user_id, price) VALUES (1, 1, 100);
INSERT INTO task2.orders (id, user_id, price) VALUES (2, 1, 50);
INSERT INTO task2.orders (id, user_id, price) VALUES (3, 2, 500);
INSERT INTO task2.orders (id, user_id, price) VALUES (4, 2, 10);