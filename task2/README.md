# TASK #2: MySQL queries

Files:

* 1-schema.sql - tables
* 2-fixture - data
* test-{1-3}.sql - queries
    * users with multiple emails
    * users with no orders
    * users with more than 2 orders


Test
```shell script
make test
```